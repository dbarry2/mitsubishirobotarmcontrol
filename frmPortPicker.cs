﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PCComm;

namespace Robot_Control_V1
{
    public partial class frmPortPicker : Form
    {
        private frmMain _parentRef;

        public frmPortPicker(frmMain parentRef)
        {
            InitializeComponent();
            this._parentRef = parentRef;
        }

        private void butOK_Click(object sender, EventArgs e)
        {
            if (radParallel.Checked)
            {
                _parentRef.SetPortType(CommsPortType.Parallel);
                _parentRef.SetPortName(cmbParallelPortName.Text);
            }
            else if (radSerial.Checked)
            {
                _parentRef.SetPortType(CommsPortType.RS232);
                _parentRef.SetPortName(cmbSerialPortName.Text);
            }

            this.Close();
            this.Dispose();
        }

        private void frmPortPicker_Load(object sender, EventArgs e)
        {
            string[] serialPorts = System.IO.Ports.SerialPort.GetPortNames();

            this.cmbSerialPortName.Items.Clear();
            this.cmbSerialPortName.Items.AddRange(serialPorts);



            switch (_parentRef.KnownPortType)
            {
                case CommsPortType.Parallel:
                    this.cmbParallelPortName.Text = _parentRef._knownPort;
                    radParallel.Checked = true;
                    radSerial.Checked = false;
                    break;
                case CommsPortType.RS232:
                    this.cmbSerialPortName.Text = _parentRef._knownPort;
                    radParallel.Checked = false;
                    radSerial.Checked = true;
                    break;
            }
        }
    }
}
