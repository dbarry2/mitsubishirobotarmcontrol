﻿namespace Robot_Control_V1
{
    partial class frmPortPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbParallelPortName = new System.Windows.Forms.ComboBox();
            this.butOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbSerialPortName = new System.Windows.Forms.ComboBox();
            this.radSerial = new System.Windows.Forms.RadioButton();
            this.radParallel = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbParallelPortName
            // 
            this.cmbParallelPortName.FormattingEnabled = true;
            this.cmbParallelPortName.Items.AddRange(new object[] {
            "LPT1",
            "LPT2",
            "LPT3"});
            this.cmbParallelPortName.Location = new System.Drawing.Point(119, 42);
            this.cmbParallelPortName.Name = "cmbParallelPortName";
            this.cmbParallelPortName.Size = new System.Drawing.Size(121, 21);
            this.cmbParallelPortName.TabIndex = 0;
            this.cmbParallelPortName.Text = "LPT1";
            // 
            // butOK
            // 
            this.butOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.butOK.Location = new System.Drawing.Point(205, 153);
            this.butOK.Name = "butOK";
            this.butOK.Size = new System.Drawing.Size(75, 23);
            this.butOK.TabIndex = 2;
            this.butOK.Text = "OK";
            this.butOK.UseVisualStyleBackColor = true;
            this.butOK.Click += new System.EventHandler(this.butOK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cmbSerialPortName);
            this.groupBox1.Controls.Add(this.radSerial);
            this.groupBox1.Controls.Add(this.radParallel);
            this.groupBox1.Controls.Add(this.cmbParallelPortName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 135);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Communications Port";
            // 
            // cmbSerialPortName
            // 
            this.cmbSerialPortName.FormattingEnabled = true;
            this.cmbSerialPortName.Location = new System.Drawing.Point(119, 87);
            this.cmbSerialPortName.Name = "cmbSerialPortName";
            this.cmbSerialPortName.Size = new System.Drawing.Size(121, 21);
            this.cmbSerialPortName.TabIndex = 4;
            // 
            // radSerial
            // 
            this.radSerial.AutoSize = true;
            this.radSerial.Location = new System.Drawing.Point(20, 87);
            this.radSerial.Name = "radSerial";
            this.radSerial.Size = new System.Drawing.Size(93, 17);
            this.radSerial.TabIndex = 3;
            this.radSerial.TabStop = true;
            this.radSerial.Text = "Serial (RS232)";
            this.radSerial.UseVisualStyleBackColor = true;
            // 
            // radParallel
            // 
            this.radParallel.AutoSize = true;
            this.radParallel.Location = new System.Drawing.Point(20, 43);
            this.radParallel.Name = "radParallel";
            this.radParallel.Size = new System.Drawing.Size(59, 17);
            this.radParallel.TabIndex = 2;
            this.radParallel.TabStop = true;
            this.radParallel.Text = "Parallel";
            this.radParallel.UseVisualStyleBackColor = true;
            // 
            // frmPortPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 188);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.butOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPortPicker";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = ".";
            this.Load += new System.EventHandler(this.frmPortPicker_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbParallelPortName;
        private System.Windows.Forms.Button butOK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbSerialPortName;
        private System.Windows.Forms.RadioButton radSerial;
        private System.Windows.Forms.RadioButton radParallel;
    }
}