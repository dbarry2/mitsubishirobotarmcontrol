﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;
using PCComm;
using System.IO.Ports;

namespace Robot_Control_V1
{
    public partial class frmMain : Form
    {

        public string Version = "1.5";
        public int DefaultParallelPortAddress = 888;

        internal CommsPortType _knownPortType = CommsPortType.RS232;
        internal string _knownPort = "COM1";
        private string _knownFileName = "";

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Text = "Robot Control V" + this.Version;
            txtCommandScript.Font = new System.Drawing.Font("Courier New", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            txtCommandImmediate.Font = new System.Drawing.Font("Courier New", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            UpdateLineCount();
            UpdatePortStatus();
        }

        public CommsPortType KnownPortType
        {
            get { return _knownPortType; }
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog f = new FontDialog();
            f.Font = txtCommandScript.Font;
            if(f.ShowDialog() == DialogResult.OK)
                txtCommandScript.Font = f.Font;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void butSendScript_Click(object sender, EventArgs e)
        {
            SendScript((string) txtCommandScript.Text.Clone());
        }

        private void SendScript(string script)
        {
            switch (_knownPortType)
            {
                case CommsPortType.Parallel:

                    SendMultiLineStringToParallelPort(script, _knownPort);
                    break;
                case CommsPortType.RS232:
                    SendMultiLineStringToSerialPort(script, _knownPort);
                    break;
            }

        }

        private void SendMultiLineStringToSerialPort(string script, string _knownPort)
        {
            MessageBox.Show(script, _knownPort);

            CommunicationManager commMgr = new CommunicationManager(); //"9600","None","One",7,_knownPort);

            commMgr.BaudRate = "9600";
            commMgr.DataBits = "7";
            commMgr.Parity = "None";
            commMgr.PortName = _knownPort;
            commMgr.StopBits = "One";
            commMgr.WriteData(script);

        }

        private void SendMultiLineStringToParallelPort(string script, string knownPort)
        {
            string tempdir = GetWindowsTempDir();
            string tempfile = tempdir + "\\robotscript.rbt";

            try
            {

                if (File.Exists(tempfile))
                    File.Delete(tempfile);

                string cleanscript = script.Replace(Environment.NewLine, "\n").ToUpper();

                if (!cleanscript.EndsWith("\n"))
                    cleanscript += "\n";

                using (TextWriter tw = new StreamWriter(tempfile))
                {
                    tw.Write(cleanscript);
                    tw.Close();
                }

                string command = "type " + tempfile + " > " + knownPort;
                string tempfile2 = tempdir + "\\rbottemp.bat";

                if (File.Exists(tempfile2))
                    File.Delete(tempfile2);

                using (TextWriter tw2 = new StreamWriter(tempfile2))
                {
                    tw2.WriteLine(command);
                    tw2.Close();
                }

                ProcessStartInfo psi = new ProcessStartInfo();
                psi.CreateNoWindow = true;
                psi.FileName = tempfile2;
                psi.UseShellExecute = false;

                Console.WriteLine(command);

                try
                {
                    Process.Start(psi);
                }
                catch (Exception e2)
                {
                    throw e2;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Error sending commands. Do not click the send buttons so fast!");
            }
        }

        private void butSend_Click(object sender, EventArgs e)
        {
            string script = (string)txtCommandImmediate.Text.Clone();

            SendScript(script);
        }

        private void SendStringToParallelPort(string script, string knownPort)
        {
            if (!script.EndsWith("\n"))
                script += "\n";

            SendMultiLineStringToParallelPort(script, knownPort);

        }

        private string GetWindowsTempDir()
        {
            return (Environment.GetEnvironmentVariable("TEMP"));
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure? The current script will be lost. " + Environment.NewLine + "Please make sure you save this first if it is needed.", "Start new script?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // theyre sure
                txtCommandScript.Text = "";
            }
            else
            {
                // cancel!
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (!String.IsNullOrEmpty(_knownFileName))
            //{
                if (AskToSave())
                {
                    SaveNow(_knownFileName);
                }
            //}
            //else
            //{
            //    SaveNow(_knownFileName);
            //}
        }

        private void SaveNow(string file)
        {
            StreamWriter writer = new StreamWriter(file,false);
            writer.Write(txtCommandScript.Text);
            writer.Close();
        }

        private bool AskToSave()
        {
            SaveFileDialog s = new SaveFileDialog();
            s.Filter = "Robot Control Files (*.rbt)|*.rbt|Text Files (*.txt)|*.txt|Any File (*.*)|*.*";
            s.Title = "Save Robot Control Instruction File";
            if (s.ShowDialog() == DialogResult.OK)
            {
                _knownFileName = s.FileName;
                return true;
            }
            s.Dispose();
            return false;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool decision = true;

            if (!string.IsNullOrEmpty(txtCommandScript.Text))
            {
                if (MessageBox.Show("Are you sure? The current script will be lost. " + Environment.NewLine + "Please make sure you save this first if it is needed.", "Open script?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    decision = true;
                }
                else
                {
                    // cancel!
                    decision = false;
                    return;
                }
            }

            if (decision)
            {
                // theyre sure
                OpenFileDialog o = new OpenFileDialog();
                if (o.ShowDialog() == DialogResult.OK)
                {
                    Stream file = o.OpenFile();
                    StreamReader reader = new StreamReader(file);
                    char[] data = new char[file.Length];
                    reader.ReadBlock(data, 0, (int)file.Length);
                    txtCommandScript.Text = new String(data);
                    reader.Close();
                }
            }
            else
            {
                // cancel!
                return;
            }
        }

        internal void SetPortName(string portName)
        {
            _knownPort = portName;
            UpdatePortStatus();
        }

        private void portToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPortPicker portPicker = new frmPortPicker(this);
            portPicker.ShowDialog();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtCommandScript.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtCommandScript.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtCommandScript.Paste();
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void txtCommandScript_TextChanged(object sender, EventArgs e)
        {
            UpdateLineCount();
        }

        private void UpdateLineCount()
        {
            string[] lines = txtCommandScript.Text.Split((new string[] { Environment.NewLine }), StringSplitOptions.None);
            toolStripStatusLabel1.Text = "Line count: " + lines.Length.ToString();
        }

        private void butNest_Click(object sender, EventArgs e)
        {
            SendScript("NT");
        }

        private void butGripOpen_Click(object sender, EventArgs e)
        {
            SendScript("GO");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SendScript("GC");
        }

        //internal void FindText(string searchText)
        //{
        //    txtCommandScript.Text.IndexOf(searchText,txtCommandScript.sele
        //}

        internal void SetPortType(CommsPortType commsPortType)
        {
            _knownPortType = commsPortType;
        }

        private void UpdatePortStatus()
        {
            tsslPort.Text = _knownPortType.ToString() + " " +_knownPort;
        }
    }
}
